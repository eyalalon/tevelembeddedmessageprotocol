sudo rm -r flatbuffers/
git clone https://github.com/google/flatbuffers
cd flatbuffers
echo "Building flatbuffers Cmake project"
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
echo "Building flatbuffer compiler: flatc"
make flatc
cd ..
echo "Creating CPP TevelMsgProtocol"
./flatbuffers/flatc --cpp TevelMsgProtocol.fbs
echo "Creating Python TevelMsgProtocol"
./flatbuffers/flatc --python TevelMsgProtocol.fbs
echo "Finished Successfully"
cd TevelMsgProtocol
echo "Making symbolic link to flatbuffers in TevelMsgProtocol"
ln -s ../flatbuffers/python/flatbuffers flatbuffers
cd ..
make clean
echo "Building TevelMsgProtocol Server and Client testers for CPP"
make