cmake_minimum_required(VERSION 3.13)
project(testcppMEP)

set(CMAKE_CXX_STANDARD 14)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

add_executable(testcppMEP main.cpp)

target_link_libraries(testcppMEP
        ${CONAN_LIBS})