#include"TevelMsgAssistant.hpp"
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include<unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include<iostream>
#define PORT 8080
#define SA struct sockaddr

using namespace TevelMsgProtocol;

//Forward Declarations
int ConnectToServerAndReturnServerSocketFd();

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
int main()
{
    int sizeOfTvlMsg = -1;
    TevelMsgAssistant assistant(100);
    uint8_t* tvlMsgBuf = assistant.EncodeTevelMsgBuf(SourceToTarget_ClientToPicktool, CommandType_DoLeftWithPwm, 1234567, 7654321, &sizeOfTvlMsg);
    int serverFd = ConnectToServerAndReturnServerSocketFd();
    write(serverFd, (char*)&sizeOfTvlMsg, sizeof(sizeOfTvlMsg));
    std::cout << "Writing size of message to server, " << sizeOfTvlMsg << std::endl;
    std::cout << "Writing message to server" << std::endl;
    write(serverFd, tvlMsgBuf, sizeOfTvlMsg);
    std::cout << "Closing socket" << std::endl;
    close(serverFd);
    std::cout << "Exiting" << std::endl;
    return 0;
}
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
int ConnectToServerAndReturnServerSocketFd()
{
    int connfd;
    struct sockaddr_in servaddr, cli;
    connfd = socket(AF_INET, SOCK_STREAM, 0);
    if (connfd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);

    if (connect(connfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");

    return connfd;
}