from conans import ConanFile, CMake, tools
import os
import TevelSemanticRelease

__version__ = "1.0.0"

class testcppMEPServer(ConanFile):
    name = "testcppMEPClient"
    version = __version__
    license = "Tevel"
    author = "Eyal eyal.a@tevel-tech.com"
    description = "picktool driver"
    topics = ("HAL", "picktool", "driver", "twister", "gripper")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = "*"
    requires = "masterEyalProtocol/1.0.0@tevel/masterEyalProtocol"
    build_requires = "cmake_installer/[3.13.*]@conan/stable"

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder=".")
        cmake.build()

    def package(self):
        self.copy("*.h*", dst="include", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("bin/*", dst="bin", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["testcppMEPClient"]

