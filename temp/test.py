import socket
import sys
import struct
import time
from MasterEyalProtocol import TevelMsgAssistant
from MasterEyalProtocol.TevelMsgProtocol import SourceToTarget, CommandType

# CREATE PICKTOOL MSG
assistant = TevelMsgAssistant.TevelMsgAssistant(100)
bufMsg = assistant.encode_tevel_msg(SourceToTarget.SourceToTarget().ClientToPicktool
                                , CommandType.CommandType().DoLeftWithPwm
                                , 6574839
                                , 9384756)

#INIT CONNECTION TO SERVER
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('127.0.0.1', 8080)
print >>sys.stderr, 'connecting to %s port %s' % server_address
sock.connect(server_address)

#SEND PICKTOOL MESSAGE TO SERVER
sizeOfMsg = struct.pack('<I', len(bufMsg))
print >>sys.stderr, 'sending size of Msg "%d"' % len(bufMsg)
sock.sendall(sizeOfMsg)
time.sleep(0.1)
print >>sys.stderr, 'sending Msg'
sock.sendall(bufMsg)
sock.close()
