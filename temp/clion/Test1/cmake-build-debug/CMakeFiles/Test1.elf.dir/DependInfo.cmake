# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/home/eyal/EA/Test1/startup/startup_stm32l432xx.s" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/startup/startup_stm32l432xx.s.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "STM32L432xx"
  "USE_HAL_DRIVER"
  "__packed=__attribute__((__packed__))"
  "__weak=__attribute__((weak))"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy"
  "../Drivers/CMSIS/Device/ST/STM32L4xx/Include"
  "../Drivers/CMSIS/Include"
  "/home/eyal/.conan/data/masterEyalProtocol/1.0.0/tevel/masterEyalProtocol/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include"
  "/home/eyal/.conan/data/flatbuffers/1.11.0/google/stable/package/d0ec62fc032e5a10524fa454546aa1bdbb22baf8/include"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart.c.obj"
  "/home/eyal/EA/Test1/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart_ex.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart_ex.c.obj"
  "/home/eyal/EA/Test1/Src/stm32l4xx_hal_msp.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Src/stm32l4xx_hal_msp.c.obj"
  "/home/eyal/EA/Test1/Src/stm32l4xx_it.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Src/stm32l4xx_it.c.obj"
  "/home/eyal/EA/Test1/Src/syscalls.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Src/syscalls.c.obj"
  "/home/eyal/EA/Test1/Src/system_stm32l4xx.c" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Src/system_stm32l4xx.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "STM32L432xx"
  "USE_HAL_DRIVER"
  "__packed=__attribute__((__packed__))"
  "__weak=__attribute__((weak))"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy"
  "../Drivers/CMSIS/Device/ST/STM32L4xx/Include"
  "../Drivers/CMSIS/Include"
  "/home/eyal/.conan/data/masterEyalProtocol/1.0.0/tevel/masterEyalProtocol/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include"
  "/home/eyal/.conan/data/flatbuffers/1.11.0/google/stable/package/d0ec62fc032e5a10524fa454546aa1bdbb22baf8/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/eyal/EA/Test1/Src/main.cpp" "/home/eyal/EA/Test1/cmake-build-debug/CMakeFiles/Test1.elf.dir/Src/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STM32L432xx"
  "USE_HAL_DRIVER"
  "__packed=__attribute__((__packed__))"
  "__weak=__attribute__((weak))"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy"
  "../Drivers/CMSIS/Device/ST/STM32L4xx/Include"
  "../Drivers/CMSIS/Include"
  "/home/eyal/.conan/data/masterEyalProtocol/1.0.0/tevel/masterEyalProtocol/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include"
  "/home/eyal/.conan/data/flatbuffers/1.11.0/google/stable/package/d0ec62fc032e5a10524fa454546aa1bdbb22baf8/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
