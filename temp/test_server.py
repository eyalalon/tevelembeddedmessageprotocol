import socket
import sys
import struct
import time
from MasterEyalProtocol import TevelMsgAssistant

#INIT SERVER
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('127.0.0.1', 8080)
print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(server_address)
sock.listen(1)
conn, addr = sock.accept()
sizeOfMsg = int(struct.unpack('<I', (conn.recv(4))[:4])[0])
time.sleep(0.1)
msg = conn.recv(sizeOfMsg)

if not msg:
    print "data not received well"
    exit()
assistant = TevelMsgAssistant.TevelMsgAssistant(100)
picktoolMsg = assistant.decode_tevel_msg_buf(msg)
print assistant.get_source_to_target_name(picktoolMsg.SourceToTarget())
print assistant.get_command_type_name(picktoolMsg.CommandType())
print picktoolMsg.DataOne()
print picktoolMsg.DataTwo()

conn.close()
sock.close()
