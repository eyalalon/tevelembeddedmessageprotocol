import serial
import os
import time
import struct
import json
import termios
import fcntl
import array
import sys
import struct
import time
import TevelMsgProtocol
from TevelMsgProtocol import flatbuffers
from TevelMsgProtocol import TevelMsg
from TevelMsgProtocol import SourceToTarget
from TevelMsgProtocol import CommandType
import TevelMsgAssistant



def test():
    serial_channel = serial.Serial('/dev/ttyUSB0', 921600, timeout=0.03)
    serial_channel_file_descriptor = serial_channel.fileno()
    buf = array.array('i', [0] * 32)
    fcntl.ioctl(serial_channel_file_descriptor, termios.TIOCGSERIAL, buf)
    buf[4] |= 0x2000
    fcntl.ioctl(serial_channel_file_descriptor, termios.TIOCSSERIAL, buf)

    # CREATE PICKTOOL MSG
    assistant = TevelMsgAssistant.TevelMsgAssistant(100)
    bufMsg = assistant.encode_tevel_msg(SourceToTarget.SourceToTarget().ClientToPicktool
                            , CommandType.CommandType().DoLeftWithPwm
                            , 4433221
                            , 1122334)
    # SEND PICKTOOL MSG
    sizeOfMsg = struct.pack('<I', len(bufMsg))
    print >>sys.stderr, 'sending size of Msg "%d"' % len(bufMsg)
    start_time = time.time()
    serial_channel.write(sizeOfMsg)
    print >>sys.stderr, 'sending Msg'
    serial_channel.write(bufMsg)
    # RECEIVE PICKTOOL MSG
    sizeToRcv = int(struct.unpack('<I', (serial_channel.read(4))[:4])[0])
    msgReceived = serial_channel.read(sizeToRcv)
    end_time = time.time()
    print 'Time elapsed: ' + str(end_time - start_time)
    picktoolMsg = assistant.decode_tevel_msg_buf(msgReceived)
    print assistant.get_source_to_target_name(picktoolMsg.SourceToTarget())
    print assistant.get_command_type_name(picktoolMsg.CommandType())
    print picktoolMsg.DataOne()
    print picktoolMsg.DataTwo()
    pass


if __name__ == '__main__':
    test()


