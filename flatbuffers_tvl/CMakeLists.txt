cmake_minimum_required(VERSION 3.13)
project(flatbuffers_tvl)

set(CMAKE_CXX_STANDARD 14)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

execute_process (
        RESULT_VARIABLE test
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        COMMAND ${CONAN_BIN_DIRS_FLATBUFFERS}/flatc --python -o MasterEyalProtocol/MasterEyalProtocolPython/MasterEyalProtocol MasterEyalProtocol/MasterEyalProtocol.fbs
        COMMAND ${CONAN_BIN_DIRS_FLATBUFFERS}/flatc --cpp -o MasterEyalProtocol/MasterEyalProtocolCpp/MasterEyalProtocol MasterEyalProtocol/MasterEyalProtocol.fbs
)

message("${test}")

add_executable(flatbuffers_tvl main.cpp)