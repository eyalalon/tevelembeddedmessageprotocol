from conans import ConanFile, CMake, tools
# import os
# import TevelSemanticRelease

__version__ = "1.2.7"

class FlatBufferConan(ConanFile):
    name = "flatbuffer"
    version = __version__
    license = "Tevel"
    author = "Or or@tevel-tech.com"
    description = "camera driver init all cameras specified in config file"
    topics = ("HAL", "camera", "driver")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=True"
    generators = "cmake"
    exports_sources = "*"
    requires = "flatbuffers/1.11.0@google/stable"

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder=".")
        cmake.build()

    def package(self):
        self.copy("*.h*", dst="include", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("bin/*", dst="bin", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["flatbuffers"]
