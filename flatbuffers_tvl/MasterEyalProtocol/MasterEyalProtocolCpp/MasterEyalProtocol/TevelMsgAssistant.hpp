#ifndef __TEVEL_MESSAGE_ASSISTANT_HPP__
#define __TEVEL_MESSAGE_ASSISTANT_HPP__
#include"MasterEyalProtocol_generated.h"
namespace TevelMsgProtocol
{
    using namespace flatbuffers;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    class TevelMsgAssistant
    {
    public:
        TevelMsgAssistant(size_t _sizeOfBuffer);
        ~TevelMsgAssistant();
        const TevelMsg* DecodeTevelMsgBuf(uint8_t* _charBuffer);
        uint8_t* EncodeTevelMsgBuf(SourceToTarget _s2t, CommandType _ct, int _d1, int _d2, int* _resultMsgSize); // _resultMsgSize is in/out, returns size of output buffer in bytes
        SourceToTarget GetSourceToTarget(const TevelMsg* _tevelMsgPtr);
        const char* GetSourceToTargetName(SourceToTarget _s2t);
        CommandType GetCommandType(const TevelMsg* _tevelMsgPtr);
        const char* GetCommandTypeName(CommandType _ct);
        int GetData1(const TevelMsg* _tevelMsgPtr);
        HandshakeData GetData1AsHandshakeData(const TevelMsg* _tevelMsgPtr);
        int GetData2(const TevelMsg* _tevelMsgPtr);
    protected:
        bool IsNullPtr();
        size_t m_sizeOfFlatBuf;
        FlatBufferBuilder m_flatBufBuilder;
        TevelMsgBuilder m_tevelMsgBuilder;
    private:
        TevelMsgAssistant(const TevelMsgAssistant& _noncopyable);
        TevelMsgAssistant& operator=(const TevelMsgAssistant& _noncopyable);
    };
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    TevelMsgAssistant::TevelMsgAssistant(size_t _sizeOfBuffer) : m_sizeOfFlatBuf(_sizeOfBuffer)
            , m_flatBufBuilder(m_sizeOfFlatBuf)
            , m_tevelMsgBuilder(m_flatBufBuilder)
    {

    }

    TevelMsgAssistant::~TevelMsgAssistant()
    {

    }

    const TevelMsg* TevelMsgAssistant::DecodeTevelMsgBuf(uint8_t* _charBuffer)
    {
        if (!_charBuffer)
        {
            return NULL;
        }
        return GetTevelMsg(_charBuffer);
    }

    uint8_t* TevelMsgAssistant::EncodeTevelMsgBuf(SourceToTarget _s2t, CommandType _ct, int _d1, int _d2, int* _resultMsgSize) // _resultMsgSize is in/out, returns size of output buffer in bytes
    {
        if (!_resultMsgSize)
        {
            return NULL;
        }
        m_tevelMsgBuilder.add_source_to_target(_s2t);
        m_tevelMsgBuilder.add_command_type(_ct);
        m_tevelMsgBuilder.add_data_one(_d1);
        m_tevelMsgBuilder.add_data_two(_d2);
        Offset<TevelMsgProtocol::TevelMsg> tempMsg = m_tevelMsgBuilder.Finish();
        TevelMsgProtocol::FinishTevelMsgBuffer(m_flatBufBuilder, tempMsg);
        uint8_t* msg = m_flatBufBuilder.GetBufferPointer();
        *_resultMsgSize = m_flatBufBuilder.GetSize();
        return msg;
    }

    SourceToTarget TevelMsgAssistant::GetSourceToTarget(const TevelMsg* _tevelMsgPtr)
    {
        if (!_tevelMsgPtr)
        {
            return SourceToTarget::SourceToTarget_InitValue;
        }
        return _tevelMsgPtr->source_to_target();
    }

    const char* TevelMsgAssistant::GetSourceToTargetName(SourceToTarget _s2t)
    {
        return EnumNameSourceToTarget(_s2t);
    }

    CommandType TevelMsgAssistant::GetCommandType(const TevelMsg* _tevelMsgPtr)
    {
        if (!_tevelMsgPtr)
        {
            return CommandType::CommandType_InitValue;
        }
        return _tevelMsgPtr->command_type();
    }

    const char* TevelMsgAssistant::GetCommandTypeName(CommandType _ct)
    {
        return EnumNameCommandType(_ct);
    }

    int TevelMsgAssistant::GetData1(const TevelMsg* _tevelMsgPtr)
    {
        if (!_tevelMsgPtr)
        {
            return -1;
        }
        return _tevelMsgPtr->data_one();
    }

    HandshakeData GetData1AsHandshakeData(const TevelMsg* _tevelMsgPtr)
    {
        HandshakeData handshakeData = HandshakeData::HandshakeData_InitValue;
        if (!_tevelMsgPtr)
        {
            return handshakeData;
        }
        handshakeData = (HandshakeData)_tevelMsgPtr->data_one();
        return handshakeData;
    }

    int TevelMsgAssistant::GetData2(const TevelMsg* _tevelMsgPtr)
    {
        if (!_tevelMsgPtr)
        {
            return -1;
        }
        return _tevelMsgPtr->data_two();
    }

}//namespace TevelMsgProtocol
#endif//#ifndef __TEVEL_MESSAGE_ASSISTANT_HPP__
