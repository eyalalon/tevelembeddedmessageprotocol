from conans import ConanFile, CMake, tools
# import os
import TevelSemanticRelease

__version__ = "1.0.0"

class MasterEyalProtocol(ConanFile):
    name = "masterEyalProtocol"
    version = __version__
    exports_sources = "*"
    no_copy_source = True
    requires = "flatbuffers/1.11.0@google/stable"

    def package(self):
        self.copy("*.h*", dst="include", keep_path=False)

    def package_id(self):
        self.info.header_only()
