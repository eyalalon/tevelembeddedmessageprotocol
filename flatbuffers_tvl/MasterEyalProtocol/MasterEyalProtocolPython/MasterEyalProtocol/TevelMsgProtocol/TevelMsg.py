# automatically generated by the FlatBuffers compiler, do not modify

# namespace: TevelMsgProtocol

import flatbuffers

class TevelMsg(object):
    __slots__ = ['_tab']

    @classmethod
    def GetRootAsTevelMsg(cls, buf, offset):
        n = flatbuffers.encode.Get(flatbuffers.packer.uoffset, buf, offset)
        x = TevelMsg()
        x.Init(buf, n + offset)
        return x

    # TevelMsg
    def Init(self, buf, pos):
        self._tab = flatbuffers.table.Table(buf, pos)

    # TevelMsg
    def SourceToTarget(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(4))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Int32Flags, o + self._tab.Pos)
        return 0

    # TevelMsg
    def CommandType(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(6))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Int32Flags, o + self._tab.Pos)
        return 0

    # TevelMsg
    def DataOne(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(8))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Int32Flags, o + self._tab.Pos)
        return 0

    # TevelMsg
    def DataTwo(self):
        o = flatbuffers.number_types.UOffsetTFlags.py_type(self._tab.Offset(10))
        if o != 0:
            return self._tab.Get(flatbuffers.number_types.Int32Flags, o + self._tab.Pos)
        return 0

def TevelMsgStart(builder): builder.StartObject(4)
def TevelMsgAddSourceToTarget(builder, sourceToTarget): builder.PrependInt32Slot(0, sourceToTarget, 0)
def TevelMsgAddCommandType(builder, commandType): builder.PrependInt32Slot(1, commandType, 0)
def TevelMsgAddDataOne(builder, dataOne): builder.PrependInt32Slot(2, dataOne, 0)
def TevelMsgAddDataTwo(builder, dataTwo): builder.PrependInt32Slot(3, dataTwo, 0)
def TevelMsgEnd(builder): return builder.EndObject()
