# automatically generated by the FlatBuffers compiler, do not modify

# namespace: TevelMsgProtocol

class CommandType(object):
    InitValue = 0
    EOW = 1
    Reset = 2
    Handshake = 3
    Ping = 4
    ReceiveNumOfBytes = 5
    DoLeft = 6
    DoLeftWithPwm = 7
    SetLeftPwm = 8
    GetLeftPwm = 9
    DoRight = 10
    DoRightWithPwm = 11
    SetRightPwm = 12
    GetRightPwm = 13
    DoStop = 14
    DoStopWithPwm = 15
    SetStopPwm = 16
    GetStopPwm = 17
    DoOpen = 18
    DoOpenWithPwm = 19
    SetOpenPwm = 20
    GetOpenPwm = 21
    DoClose = 22
    DoCloseWithPwm = 23
    SetClosePwm = 24
    GetClosePwm = 25
    SetAutoOffTimeMs = 26
    GetAutoOffTimeMs = 27
    GetCurrent = 28
    GetTempProp1 = 29
    GetTempProp2 = 30
    OpenVacuumPump = 31
    CloseVacuumPump = 32
    GetVacuumPumpPressure = 33

