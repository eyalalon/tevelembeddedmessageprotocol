from TevelMsgProtocol import TevelMsg, SourceToTarget, CommandType
import flatbuffers
import inspect

class TevelMsgAssistant(object):
    def __init__(self, _bufferSize=100):
        self.sizeOfFlatBuf = _bufferSize
        self.flatBufBuilder = flatbuffers.Builder(self.sizeOfFlatBuf)

    def __delete__(self):
        pass

    def decode_tevel_msg_buf(self, _msgBuf):
        tevelMsg = TevelMsg.TevelMsg.GetRootAsTevelMsg(_msgBuf, 0)
        return tevelMsg

    def encode_tevel_msg(self, _sourceToTarget, _commandType, _data1, _data2):
        TevelMsg.TevelMsgStart(self.flatBufBuilder)
        TevelMsg.TevelMsgAddSourceToTarget(self.flatBufBuilder, _sourceToTarget)
        TevelMsg.TevelMsgAddCommandType(self.flatBufBuilder, _commandType)
        TevelMsg.TevelMsgAddDataOne(self.flatBufBuilder, _data1)
        TevelMsg.TevelMsgAddDataTwo(self.flatBufBuilder, _data2)
        picktoolMsg = TevelMsg.TevelMsgEnd(self.flatBufBuilder)
        self.flatBufBuilder.Finish(picktoolMsg)
        msgBuf =  self.flatBufBuilder.Output()
        return msgBuf

    def get_source_to_target(self, _tevel_msg):
        return _tevel_msg.SourceToTarget()

    def get_source_to_target_name(self, _sourceToTarget):
        selectedName = ''
        members = inspect.getmembers(SourceToTarget.SourceToTarget())
        for name, id in members:
            if id == 0:
                selectedName = name
            if id == _sourceToTarget:
                return name
        return selectedName

    def get_command_type(self, _tevel_msg):
        return _tevel_msg.CommandType()

    def get_command_type_name(self, _commandType):
        selectedName = ''
        members = inspect.getmembers(CommandType.CommandType())
        for name, id in members:
            if id == 0:
                selectedName = name
            if id == _commandType:
                return name
        return selectedName

    def get_data_one(self, _tevel_msg):
        return _tevel_msg.DataOne()

    def get_data_one_as_handshake_data(self, _tevel_msg):
        return self.get_data_one(_tevel_msg)

    def get_data_two(self, _tevel_msg):
        return _tevel_msg.DataTwo()

if __name__ == '__main__':
    assistant = TevelMsgAssistant(100)
    tvlMsgBuf = assistant.encode_tevel_msg(SourceToTarget.SourceToTarget().ClientToPicktool
                                           , CommandType.CommandType().DoLeftWithPwm
                                           , 9988776
                                           , 6677889)
    tvlMsg = assistant.decode_tevel_msg_buf(tvlMsgBuf)
    print assistant.get_source_to_target_name(assistant.get_source_to_target(tvlMsg))
    print assistant.get_command_type_name(assistant.get_command_type(tvlMsg))
    print str(assistant.get_data_one(tvlMsg))
    print str(assistant.get_data_two(tvlMsg))