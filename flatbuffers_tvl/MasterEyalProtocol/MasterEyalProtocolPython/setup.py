from setuptools import setup
import setuptools
import TevelSemanticRelease

__version__ = "1.0.0"

setup(
    name='MasterEyalProtocol',
    version=__version__,
    description='wrapps picktool reduced version with current sensor for gripper',
    author='Eyal Alon',
    author_email='eyal.a@tevel-tech.com',
    install_requires=[
        'flatbuffers'
    ],
    license='TEVEL',
    packages= setuptools.find_packages(),
    # packages=['MasterEyalProtocol'],
    # package_dir={'.': './'},
    # include_package_data=True,
    # classifiers=[
    #     'Development Status :: 5 - Production/Stable',
    #     'Intended Audience :: Science/Research',
    #     'Programming Language :: Python :: 2.7'],
)

