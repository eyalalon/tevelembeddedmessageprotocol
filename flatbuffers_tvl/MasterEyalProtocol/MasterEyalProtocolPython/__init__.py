from MasterEyalProtocol import TevelMsgAssistant
from MasterEyalProtocol import TevelMsgProtocol

__all__ = ['TevelMsgAssistant', 'TevelMsgProtocol']