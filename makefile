FLAGS=-g
SERVER_APP_NAME=ServerApp
CLIENT_APP_NAME=ClientApp
TARGETS=$(SERVER_APP_NAME).out $(CLIENT_APP_NAME).out

all: $(TARGETS)

$(SERVER_APP_NAME).out: PicktoolMsg_Server.cpp
	g++ $(FLAGS) PicktoolMsg_Server.cpp -o $(SERVER_APP_NAME).out

$(CLIENT_APP_NAME).out: PicktoolMsg_Client.cpp
	g++ $(FLAGS) PicktoolMsg_Client.cpp -o $(CLIENT_APP_NAME).out

clean:
	rm $(TARGETS)