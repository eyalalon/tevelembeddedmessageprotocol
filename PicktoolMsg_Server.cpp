#include"TevelMsgAssistant.hpp"
#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include<unistd.h>
#include<iostream>
#define PORT 8080 
#define SA struct sockaddr 

using namespace TevelMsgProtocol;

//Forward declarations
int InitServerAndReturnSocketFd();
int AcceptClientAndReturnSocketFd(int _serverFd);

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
int main()
{
    TevelMsgAssistant tvlMsgAssistant(100);
    uint8_t buff[100]; 
    int sizeOfData;
    int serverFd = InitServerAndReturnSocketFd();
    int clientFd = AcceptClientAndReturnSocketFd(serverFd);

    read(clientFd, (char*)&sizeOfData, sizeof(sizeOfData));
    read(clientFd, buff, sizeOfData); 
    
    const TevelMsg* tvlMsgPtr = tvlMsgAssistant.DecodeTevelMsgBuf(buff);
    SourceToTarget sourceToTarget = tvlMsgAssistant.GetSourceToTarget(tvlMsgPtr);
    CommandType commandType = tvlMsgAssistant.GetCommandType(tvlMsgPtr);
    int data1 = tvlMsgAssistant.GetData1(tvlMsgPtr);
    int data2 = tvlMsgAssistant.GetData2(tvlMsgPtr);

    std::cout << tvlMsgAssistant.GetSourceToTargetName(sourceToTarget) << std::endl;
    std::cout << tvlMsgAssistant.GetCommandTypeName(commandType) << std::endl;
    switch (sourceToTarget)
    {
    case SourceToTarget_ClientToPicktool:
        switch (commandType)
        {
            case CommandType_DoLeftWithPwm:
                std::cout << "Data 1 = " << data1 << std::endl;
                std::cout << "Data 2 = " << data2 << std::endl;
                break;
        }
        break;
    }

    close(clientFd); 
    close(serverFd);    
    return 0;
}
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Function definitions
int InitServerAndReturnSocketFd()
{
    int sockfd; 
    struct sockaddr_in servaddr; 
  
    // socket create and verification 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        printf("socket creation failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket successfully created..\n"); 
    bzero(&servaddr, sizeof(servaddr)); 
  
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
    servaddr.sin_port = htons(PORT); 
  
    int option;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option)) < 0)
        printf("setsockopt(SO_REUSEADDR) failed");
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
        printf("socket bind failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket successfully binded..\n"); 
  
    // Now server is ready to listen and verification 
    if ((listen(sockfd, 5)) != 0) { 
        printf("Listen failed...\n"); 
        exit(0); 
    } 
    else
        printf("Server listening..\n"); 
    return sockfd;
}

int AcceptClientAndReturnSocketFd(int _serverFd)
{
    int len; 
    struct sockaddr_in cli; 
    int clientFd = accept(_serverFd, (SA*)&cli, (socklen_t*)&len); 
    if (clientFd < 0) { 
        printf("server acccept failed...\n"); 
        exit(0); 
    } 
    else
        printf("server acccept the client...\n");
    return clientFd; 
}